<?php
    /*
    * Note: TESTED ONLY ON BITBUCKET REPOSITORIES
    * Created By: Edmar
    */

    function execute($cmd, $path)
    {
        if ($path) {
            mkdir($path, 0777, true);
            chdir($path);
        }
        $result = array();
        exec($cmd, $result);
        foreach ($result as $line) {
            print($line . "\n");
        }
    }

    if ($_GET['action']) {

        /**
        *  START OF CONFIGURATION
        **/
        $protocol = 'https'; // repository protocol
        $user = 'deram92'; // repository username
        $repository = 'files'; // repository name
        $repositoryOrigin = "bitbucket.org/{$user}/{$repository}"; // repository origin
        $branch = 'master'; // repository branch to use
        $path = 'ecshopping'; // installation path (It will install on same directory of git.php if leave empty. )
        /**
        *  END OF CONFIGURATION
        **/

        $cmd = '';
        $tips = "\n\n&nbsp;&nbsp;&nbsp;<b>List of git action available</b> \n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* init \n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* remote_add_origin \n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* pull \n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* status";

        switch ($_GET['action']) {
            case 'init':
                $cmd = "git init";
                break;
            case 'remote_add_origin':
                $cmd = "git remote add origin {$repositoryOrigin}";
                break;
            case 'pull':
                if ($_GET['pwd']) {
                    $cmd = "git pull {$protocol}://{$user}:{$_GET['pwd']}@{$repositoryOrigin} {$branch}";
                } else {
                    echo 'Pull action required password. Try again!';
                }
                break;
            case 'status':
                $cmd = "git status";
                break;
            default:
                print("<pre>Action not found. Try again! {$tips}</pre>");
        }
        
        if ($cmd) {
            print("<pre>" . execute($cmd, $path) . "</pre>");
        }
    } else {
        print("<pre>No action found. Try again! {$tips}");
    }
?>
